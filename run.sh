#!/bin/bash

SSB_PUB_DIR=$(grep DIR .env | cut -d '=' -f 2-)
mkdir $SSB_PUB_DIR
chown -R 1000:1000 $SSB_PUB_DIR
docker-compose up -d

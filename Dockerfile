FROM node:10.16

USER root
ADD https://github.com/krallin/tini/releases/download/v0.18.0/tini /tini
RUN chmod +x /tini
RUN mkdir /home/node/.npm-global ; \
    chown -R node:node /home/node/
ENV PATH=/home/node/.npm-global/bin:$PATH
ENV NPM_CONFIG_PREFIX=/home/node/.npm-global
ENV DEBUG="*"

USER node
RUN mkdir /home/node/.ssb && mkdir /home/node/.ssb/node_modules
COPY scripts/start.sh /home/node/.ssb/start.sh
COPY config /home/node/.ssb/config
RUN cd /home/node/.ssb/node_modules && git clone https://github.com/arj03/ssb-partial-replication.git && cd ssb-partial-replication && npm i
RUN cd /home/node/.ssb/node_modules && git clone https://github.com/ssbc/ssb-tunnel.git && cd ssb-tunnel && npm i
RUN cd /home/node/.ssb/node_modules && git clone https://github.com/ssbc/ssb-device-address.git && cd ssb-device-address && npm i
RUN cd /home/node/.ssb/node_modules && git clone https://github.com/ssbc/ssb-identities.git && cd ssb-identities && npm i
RUN npm install -g ssb-minimal-pub-server

EXPOSE 8008
EXPOSE 8007
EXPOSE 8989

HEALTHCHECK --interval=30s --timeout=30s --start-period=10s --retries=10 \
  CMD ssb-minimal-pub-server check || exit 1
ENV HEALING_ACTION RESTART

ENTRYPOINT [ "/home/node/.ssb/start.sh" ]

# SSB Browser Pub

## Tunnel

add to plugins in config and uncomment on Dockerfile.

```
...
"plugins": {
  "ssb-device-address": true,
  "ssb-identities": true,
  "ssb-peer-invites": true,
  "ssb-tunnel": true,
  "ssb-partial-replication": true
},

```

## Usage

Edit config with the address of your localpub.

```
docker build -t ssb-browser-pub .
```


```
docker run --name ssb-browser-pub \
  -p 8008:8008 -p 8007:8007 \
  --net=host \
  --privileged \
  -d \
  -v ~/Documentos/ssb-pub:/home/node/.ssb \
  -v /var/run/dbus:/var/run/dbus \
  ssb-browser-pub 
```

Get pub id:

```
docker exec ssb-browser-pub ssb-server whoami
```

Give the pub a name:

```
docker exec ssb-browser-pub ssb-server publish \
  --type about \
  --about {yourId} \
  --name {name}

```

Generate invite:
```
docker exec ssb-browser-pub ssb-server invite.create 1
```